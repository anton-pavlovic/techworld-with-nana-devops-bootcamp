# Techworld with Nana - DevOps Bootcamp
![head](Public/head.png)

DevOps Bootcamp is a 6-month study program for understanding the theory of DevOps processes through practice exercises and tasks.

Completed exercises and notes from this course are available in this repository.

The full course is available here: https://www.techworld-with-nana.com/devops-bootcamp

## Curriculum

01. Introduction to DevOps
02. Operating Systems & Linux Basics
03. Version Control with Git
04. Build & Package Manager Tools
05. Cloud & Infrastructure as a Service Basics
06. Artifact Repository Manager with Nexus
07. Containers with Docker
08. Build Automation - CI/CD with Jenkins
09. AWS Services
10. Container Orchestration with Kubernetes
11. Kubernetes on AWS - EKS
12. Infrastructure as Code with Terraform
13. Programming with Python
14. Automation with Python
15. Configuration Management with Ansible
16. Monitoring with Prometheus

## Notes
My notes are stored in the [Notes](https://gitlab.com/anton-pavlovic/techworld-with-nana-devops-bootcamp/-/tree/main/Notes) folder.

## Exercises
My exercises are stored in the [Exercises](https://gitlab.com/anton-pavlovic/techworld-with-nana-devops-bootcamp/-/tree/main/Exercises) folder.
