# 1 - Introduction to Version Control and Git

## What is version control?
- developers working on the same code
- code is hosted centrally = code repository
- developers aren't editing file in real-time in the remote repository like Googledocs multi-collaboration
- developers also need the code locally to start app
- developers have an entire copy of the code locally
- code is fetched from a remote repo and pushed to code repo (locally)
- developers do some changes locally and then push their back to the remote repo = other developers can fatch/download it on their computers and make his changes
- developers just push and pull changes in and from this code repository

## Basic concepts of Version Control
- developers can change the same file = Git knows how to merge changes automatically
- conficts = when the same exact location was changed in the file → Git can't fix it automatically
- Best practice: push and pull often from a remote repo = Git won't merge huge changes
  - Continuous Integration = integrate code changes frequently
- Braking changes don't affect you until pull the new code from the remote repository
- Version control keeps a history of changes = every code change is tracked
  - You can revert commits
  - Each change labelled with commit message

# 2 - Basic Concepts of Git

Git has multiple parts:
- Remote Git Repository: where the code lives. Usually has UI to interact with.
- Local Git Repository: local copy of the remote repository code. Place where your changes are saved and kept until you push them to the remote repository
- History: of code chages `$ git log`
- Staging: changes to commit. 
  - Working Dir → git add → Staging Area → git commit → Local Repo → git push → Remote Repo
- Git clients: to execute git commands. Could be UI or CLI to interact with the remote/local Git repository

# 3 - Setup Git Repository Remote and Local

## Remote Git Repository
- Different Git Repositories available (Github, GitLab, ) that are both cloud repositories that everybody can register on and host their repo
- Companies have own Git servers hosted on company servers (Bitbucket, GitLab hosted locally)
- Private vs. Public repositories - you can restrict or allow access to team memebers
- platforms IU allow to a lot

## Local Git Repository
Local repository get access to the remote repository locally from computer
- Configuration to show Git branch information in Ubuntu Terminal 
  - Installation [link](https://gist.github.com/gnppro/92fad98dc2601a33fa93227325805770)
  - add the code below to your .zshrc
  `parse_git_branch() {
git branch 2> /dev/null | sed -n -e 's/^\* \(.*\)/[\1]/p'
}
COLOR_DEF='%f'
COLOR_USR='%F{243}'
COLOR_DIR='%F{197}'
COLOR_GIT='%F{39}'
NEWLINE=$'\n'
setopt PROMPT_SUBST
export PROMPT='${COLOR_USR}%n@%M ${COLOR_DIR}%d ${COLOR_GIT}$(parse_git_branch)${COLOR_DEF}${NEWLINE}%% '`
- Git client needs to be installed
  - GUI Git client = https://git-scm.com/downloads/guis
  - Git Command line Tool = `winget install --id Git.Git -e --source winget`
- Clone remote repo:
  - Git client needs to be connected with remote platform 
  - You Need to authenticate to GitLab/GitHub/...
  - To identify your client a public SSH key must be added to the remote platform
    - Gitlab > User settings > Edit Profile > SSH keys > Add an SSH key
    - follow [Use SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/user/ssh.html)
    - see if exists SSH key pair `cd ~/.ssh/`, `ls -a`
    - generate an SSH key pair `ssh-keygen -t rsa -b 2048 -C "<comment>"`
    - add the SSH key from this output: `cat ~/.ssh/id_rsa.pub` 
    - Now you should be able to access GitLab using git client
  - clone repository: `$ git clone git@gitlab.com:anton-pavlovic/techworld-with-nana-devops-bootcamp.git`
    - Getting permission denied public key on gitlab [link](https://stackoverflow.com/questions/40427498/getting-permission-denied-public-key-on-gitlab)
    - Command line Git [link](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-your-git-username-and-set-your-email)

# 4 - Working with Git

Files in local repository can have 3 different status ([link](https://gitlab.com/anton-pavlovic/techworld-with-nana-devops-bootcamp/-/blob/main/Public/3workingwithgit.PNG)):
1. New files or changed existing files are in working directory but not in the staging area. Those changes can be add to the staging area. Everything from the staging area can be commited to local repository.
2. Files that are in the staging area appear as "Changes to be committed”
3. Files that are under "Changes not staged for commit" were added to staging before but meanwhile they were modified

- `$ git status` = status of the local git repository, it could be changes not staged for commit, untracked files)
- `$ git add <file>` or `$ git add .` = git knows that there is  a change. Selected file can be promoted to the staging area. After this command files are ready to be commited and then push to the remote repository. `$ git status`
- `$ git commit` = this command will take all the files that are in the section "changes to be commited" and push to the local repository from staging area. Now can take and push file in the local repository to the remote repository
- `$ git log` = show local history of the changes in git
- `$ git push` = push file in the local repository to the remote repository

# 5 - Initialize a Git project locally

If you already have an existing project locally, you can initialize a git repository with "git init". Then you can push it to GitLab/GitHub/...

This is a use case when existing code developed locally (private project) is at some point decided to be hosted remotely on a Git repository. Therefore, you need to move existing code to a remote repo. How that usecase works:
1. `$ git init` = transform local project to the local git repository; ".git" folder for this project was created (contains information about remote git repo, git user/email, etc.)
2. Now can commit and push the files to remote repo `$ git add .` = this will add all files to the remote repository
3. `$ git commit` = we have local commit for initial code. Information about the local repository you can check `$ git status` or `$ git log`
4. At this point no configured push destination `$ git push`. Is neede to connect remote repo where to push files.
5. Create new project = `Gitlab > Create blank project`
6. Configure where local git repo push files remotely = `$ git remote add origin git@gitlab.com:anton-pavlovic/another-new-project.git`
7. Push files `$ git push` = get error because repo is connecte (that's ok) but branch is not
8. Conect the branches, setup master branch = `$ git push --set-upstream origin master` 
9. Refresh remote repo. Connection between remote and local repo is DONE!
10. After removal folder ".git" then lost connection to remote repo = `rm -rf .git` 

# 6 - Concept of Branches

- A "main" branch (also called "master") is created by default when a new repository is initialized
- Branches are used for better collaboration
- concept of branches exists in order to cleanly divide work of different developers
- Best Practice: 1 branch for each bugfix or feature
  - Common branch names: main, dev, feature, bugfix for example bugfix/ticket-2134
- developer can create temporary branches e.g. for a feature or bugfix and work on it without worrying to break the main branch
- once code is ready (bugfixed, new feature done) developer can merge completed branch into the Master branch
- Important: Large feature branches that are open for too long, increase the chance of "merge conflicts". Therefore are better small features that do not take a lot of day or weeks to implement
- New brench is possible to create in UI or commandline

## New branch with using UI
- Create branch based on master = exactly the same code is copied from master and branched it into a new branch >> [see](https://gitlab.com/anton-pavlovic/techworld-with-nana-devops-bootcamp/-/blob/main/Public/3newbranch.PNG)
- if new branch was created then local repository does not know that new branch was created
  - `$ git branch` = show available braches 
  - `$ git pull` = pull the changes from repository
-`$ git checkout <branch name>` = switch to branch; only the current branch is affected by changes.

## New branch with using command line:
- `$ git checkout master`
- `$ git checkout -b <branch name>` = git create new branch locally and switch to that new branch
- `$ git branch` = list all branches locally
- `$ git add .` + `$ git commit` + `$ git status`
- `$ git push` wil not work because remote repo does not know this branch. Therefore is needed to push the branch itself and then the changes in the branch = `$ git push --set-upstream origin <new branch>`
- new branch is now visible on remote repo

## Master and develop branch
Common practice of projects and teams is to have two main branches:
- dev branch = intermediary master, ready for production. Code is tested and ready for the next release 
- master branch = allways ready for deployment
- during sprint: features and bugfixes go into the dev
- at the end of sprint merge develop into master

## Trunk based vs. Feature based developement
- What is diff between these two practices ([picture](https://gitlab.com/anton-pavlovic/techworld-with-nana-devops-bootcamp/-/raw/main/Public/3development.PNG)):

- **Trunk based** = having just master and then having those branches constantly merged into that
  - only master branch for continuous integration/delivery.
  - pipeline is triggered whenever feature/bugfix code is merged into master (test > build deploy)
  - deploying every single feature/bugfix

- **Feature based** = having an additional develop branch
  - features/bugfixes are collected in develop branch. And then they take this big chunk of changes at the end of the sprint, merge it to master, so they have releases once every of the spring.
  - develop branch often becomes "work in progress" branch

- The best practice in DevOps and if you want to do it in the modern way, meaning set up to Continuous integration and delivery.
- You have to go with having master branch and you dont need the intermediary dev branch anymore.
- ideal goal is to deploy changes on every merge

# 7 - Merge Requests

- Best Practice: Other developer reviews code changes before merging
  - when the developer is done with coding, other developers reviews the changes until merging it to Master
  - it is because Master has must be stable and ready for PROD (no half implemented or broken things as much as possible)
- Common use cases for **pull requests**
  - when feature implementation requires a lot of code changes
  - senior developers checks code of the junior dev until he gets merge to master
  - great chance to learn and grow from each other based on suggestion how it should be done better
  - there is some validation before adding changes to master branch
- Merge request:
  - can call pull request
  - within the UI = `<project> > Code > Branches > New merge request > <fill the form> > Submit request`
  - The assignee can approve or decline the merge request with a leaved comment.

# 8 - Deleting Branches

- Leave the branch or delete it
- best practices: 
  - Option-1(better) = delete branch after merging. If found bugs after merge then create a new branch to fix that issue.
    - branches are deleted immediately and remain only active branches.
  - Option-2 = leave the branches and then clean up at some point later.
    - You have to know which branch is active or completed
- If delete the branch in the remote repo it does not delete it locally!
  - UI gitlab.com > delete branch remotely
  - check branches locally show that remotely deleted branch still remains locally `$ git branch`
  - `git status` = still can work locally with this branch
  - have to clean up the branches locally that have been deleted remotely
  - `git pull` = shows that branch has not reference remotely
  - `git checkout master` = switch to master branch
  - `git pull` = because some changes have been made
  - `git branch -d <name of branch>` = delete branch
  - `git branch` = now remotely deleted branch was deleted also locally

# 9 - Rebase

- avoiding merge commits
- `git pull` vs. `git pull --rebase`

- Use case:
  - fist change has been made in local repo by me 
  - second change at the same time and in the same branch has been made in remote repo. 
  - it means that two developers are working in the same branche
  - My local repo does not know about changes of another developer 
  - Repo of another developer does not know about my changes

## git pull
- now if i will commit my change `git add .` + `git commit` + `git push` error will happen - push failed because there are some other changes that have been already pushed to the remote branch.
- is needed to do git pull before push my changes to remote repo.
- `git pull` = pull the changes locally and creates a new commit that the remote branch got merged into my locally branch
- `git push` = this will push two commits 1) my commit 2) this merge commit 

## git pull --rebase
- `git pull -r` = instead of `git pull` can use command git pull rebase
  - it pulls the changes from remote branche and than it stacks our changes/commit on top of that, so there is no merge commit in between
- `git add .`
- `git commit -m "another commit" `
- `git push` = command rejected, you have to do pull request first
- `git pull -r ` = instead of `git pull` is used`git pull --rebase`
  - the merge commit did not happen
- `git push`
  - there is no merge branch commit because using rebase 

# 10 - Resolving Merge Conflicts

Usecase:
  - two developers can change in the exactly same place in the exactly same file in parallel without knowing what ca  use merge conflict
    - local change: edit file locally in editor + `git add .` + `git commit - m "zmena v demo01"`
    - remote change: edit file remotely at the same line like did it locally
  - if you now locally run `git push` you get error
    - push was rejected because the remote contains work that you do not have locally. First integrate the remote changes before pushing again.
  - if you now locally run `git pull --rebase` you get an error about the conflicts
    - CONFLICT (content): Merge conflict in <name of file>
    - merge was not possible because Git does not know which version/line is correct, Git can not automatically decide which is correct
    - you have to do it manually, say Git which change stays and which gets remove

Resolve merge conflict:
  - possible in Git clients, with Git UI, in command line, editor like Visual Studio Code (prefer due to visualisation conflicts)
  - The Git Changes window shows a list of files with conflicts under Unmerged Changes. To start resolving conflicts, double-click a file. Or if you have a file with conflicts opened in the editor, you can select Open Merge Editor.
  - if merge conflicts are fixed run `git rebase --continue` and pusf changes to remote `git push`
     
# 11 - Gitignore
- don't track certain files = .gitignore file in Git repository
- after create project and initiate it as git repository we can add .gitignore file to this project (root directory)
- **.gitignore file** = exclude certain folders or files from being tracked by git
  - file/folders specific to your editor
  - something specific for every developer what other developers do not need to download in their project
- example of .gitignore file:
  > ./.idea/*  
- if folder .idea has been already commited then Git is tracking it. File .gitignore is not enough, folder need to be removed from the repository:
  - stop tracking a file = `gir rm -r --cached <folder/file>`
  - `git status` = show files for removal

# 12 - Git stash

**Use case:**
- you ave some local changes in the branch that are work in progress and at the same time you need to swit to different branch
  - `git status` = shows available changes for commit
  - there are unfinished changes, which I do not want to commit those changes because it's not done yet, code is work-in-progress state
  - I want to switch to another branch `git checkout master `
    - dot allow to switch because it says that I have local changes in curent branch
    - git does not know what to do with those local changes when I want leave current branch to another branch

**Solution:**
- commit all current changes or stash them before you switch branches
- `git stash` = it wil take my working changes in "working directory" and it will save current changes for later:
  - `git status` = now there are not changes that are active or displayed
  - `git checkout master` = now can switch to another branch without problem
- if I want to get back saved changes:
  - `git checkout <branch_name>` = switch back to the branch
  - `git status` = changes are not here
  - `git stash pop` = get the changes back. Git stash pop get back changes back to working directory

**Another use case** 
- stashing allows you to make changes to the current branch but if notice that something is not working anymore 
- you can hide changes temporary to test if it works without my code changes
- all the edited code that I added or removed will be gone
- i can test if the feature worked before I did changes
- after check I can back using `git stash pop`

# 13 - Going back in history
- available feature "history" or "log of commits"
- `git log` = bunch of commits that have been made in the current branch, history of activities
  - each commit has unique commit hash and message that identify that specific commit

**Use case:**
- exists some bug it the code that we're trying to understand how this bug happened
- at the begining we do not know which commmit caused the bug
- leter tester notices that since the changes of concrete commit the bug appeared
- instead of reproduce issue we can go back in the time and checkout the commit using its hash

**Solution:**
- `git log`
- `git checkout <commit_hash>` = we are go back to a specific project version, whole code at the time of this commit
- there are local changes therefore is not possible to checkout another commit `git status`
- stash that changes `git stash` 
- run againg `git checkout <commit_hash>`
  - branch has changed and code is in the previous state, the current state is not most up-to-date state of the branch
- it's possible to create a new branch base on the code version of this point in time
- mostly this is used for testing or to reproduce a bug
- if want to go back to the up-to-date state of the branch `git checkout <branch_name>`  

# 14 - Undoing commits

**Use case 1: Revert a commit (locally)**
- make some changes in file locally `vim README.md` + add these changes `git add .` + commit these changes `git commit -m "remove some lines"` + commit is only locally, not pushed to remote repository
- Change that I made is wrong and I want to revert the changem the whole commit

**Solution 1:**
- since the change is only locally (not in remote repository) is possible to reset the last commit:
  - `git log` and see HEAD which is pointer to the last commit
  - `git reset --hard HEAD~<number>` = set how many latest commits you want to revert
    - `--hard` = reverts the commit and also discards the changes that were in commit
  - again `git log` you see that some commit(s) have disappeared and file are back in the previous state

**Use case 2: Correct a commit (locally)**
- I don't want to discard all the changes when I revert the commit.
- I realize that I still need to do the commit and those changes but I want to corrent something in my changes and then commit again. In this case can do reset head without hard.

**Solution 2:**
- `git reset --soft HEAD~1` = --soft is a default option
- `git status` can see that some that the change is still there
- `git log` can see that commit is gone
- commit got reverted but I still have code changes locally
- can correct the file, `git add .` and again `git commit ...`

**Use case 3: Amend a commit (locally)**

**Solution 3:**

# 15 -

# 16 -

